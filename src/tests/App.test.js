import { selectLoanId, fetchLoansIfNeeded } from '../actions'
import { REQUEST_LOANS, RECEIVE_LOANS, INVALIDATE_LOANS, SET_POLL, SELECT_LOAN_ID } from '../actions'

import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import nock from 'nock'
import {API_DATA_URL, STORY_DOESNT_EXIST} from '../constants'
import {sampleApiData} from '../constants/sampleApiData'

/**
 * check an action creator
 */
describe('actions', () => {
  it('should select a loan given an id', () => {
    const selectedLoanId = 12345
    const expectedAction = {
      type: SELECT_LOAN_ID,
      selectedLoanId
    }

    expect(selectLoanId(selectedLoanId)).toEqual(expectedAction)
  })
})

/**
 * check async actions
 */
const middlewares = [ thunk ]
const mockStore = configureMockStore(middlewares)

describe('async actions', () => {
  afterEach(() => {
    nock.cleanAll()
  })

  it('fetches and receives loans', () => {
    nock(API_DATA_URL)
        .get('/')
        .reply(200, sampleApiData)

    const expectedActions = [
      { type: REQUEST_LOANS },
      { type: RECEIVE_LOANS, items: sampleApiData }
    ]
    const store = mockStore({
      loans: {
        items: []
      }
    })

    return store.dispatch(fetchLoansIfNeeded())
        .then(() => { // return of async actions
          expect(store.getActions()).toMatchObject(expectedActions)
          expect(store.getActions()[1]['items']).toEqual(sampleApiData)
        })
  })
})

/**
 * check our limit story function
 */
import {sortLoans,formatStory} from '../utils'
describe('Loan list', () => {
  it('it appropriately limits stories to 20 words and 200 characters', () => {
    // check that > 20 word story gets limited and ... added
    const twentyOneWordStory = 'a b c d e f g h i j k l m n o p q r s t u'
    expect(formatStory(twentyOneWordStory)).toEqual('a b c d e f g h i j k l m n o p q r s t...')

    // check that > 200 character story gets limited and ... added
    const twoHundredOneCharStory = 'nykzrxdqnoculyqngaslhzbxkssythiidswclvkjpjpdrqnlwbbfboyfhrwluwfbujmwkwbdtqbgwjzreecp' +
        'xcsxemaycosdpqlqzecfurukroatxrdnjcsidshdezdqfqkshglehapimbtenjynlkyphpebghozjinksjbctrjrndihpqjcbzvhqfmleetkqvzfelauX'
    const trimmedCharStory = 'nykzrxdqnoculyqngaslhzbxkssythiidswclvkjpjpdrqnlwbbfboyfhrwluwfbujmwkwbdtqbgwjzreecp' +
    'xcsxemaycosdpqlqzecfurukroatxrdnjcsidshdezdqfqkshglehapimbtenjynlkyphpebghozjinksjbctrjrndihpqjcbzvhqfmleetkqvzfelau'

    expect(twoHundredOneCharStory.length).toEqual(201)
    expect(formatStory(twoHundredOneCharStory)).toEqual(trimmedCharStory + '...')

    // check that >20 word story and >200 character story gets limited and ... added
    const bigStory = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ultrices vestibulum elementum. Maecenas sed sem vitae ex' +
        ' pulvinar fringilla ornareeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee non justo. Proin sed congue neque. Cras mollis commodo ligula, vitae bibendum leo venenatis nec. Praesent' +
        ' fermentum commodo enim a vehicula. Phasellus porta mi ac mauris mollis ornare nec eget.'
    expect(bigStory.length).toEqual(395) // 395 chars
    expect(bigStory.split(' ').length).toEqual(50) // 50 words
    let storyTrimmedByWords = bigStory.split(' ').splice(0,20).join(' ')
    expect(storyTrimmedByWords.length).toEqual(201) // 201 chars after limiting to words
    expect(formatStory(bigStory)).toEqual(storyTrimmedByWords.substr(0,200) + '...')

    // check that small story doesn't get limited
    const smallStory = 'hello world'
    expect(formatStory(smallStory)).toEqual(smallStory)
  })

  it('handles a null story', () => {
    expect(formatStory(null)).toBe(STORY_DOESNT_EXIST)
  })

  it('sorts loans correctly by rating', () => {
    const unsortedLoans = [
      {name: 'Loan 1', rating: 'AAA'},
      {name: 'Loan 2', rating: 'Z'},
      {name: 'Loan 3', rating: 'AA'},
      {name: 'Loan 4', rating: 'C'},
      {name: 'Loan 5', rating: 'D'},
    ]
    const sortedLoans = [
      {name: 'Loan 3', rating: 'AA'},
      {name: 'Loan 1', rating: 'AAA'},
      {name: 'Loan 4', rating: 'C'},
      {name: 'Loan 5', rating: 'D'},
      {name: 'Loan 2', rating: 'Z'},
    ]
    expect(sortLoans(unsortedLoans, 'rating'), sortedLoans)
  })
})
