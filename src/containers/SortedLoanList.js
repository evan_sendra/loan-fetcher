import { connect } from 'react-redux'
import _ from 'lodash'
import 'locale-compare-polyfill'
import LoanList from '../components/LoanList'
import { selectLoanId } from '../actions'
import {sortLoans} from '../utils'

const mapStateToProps = (state, ownProps) => {
  return {
    loans: {
      items: sortLoans(state.loans.items, ownProps.sortParameter)
    }
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onLoanClick: (id) => {
      dispatch(selectLoanId(id))
    }
  }
}

const SortedLoanList = connect(
  mapStateToProps,
  mapDispatchToProps
)(LoanList)

export default SortedLoanList
