import React from 'react'
import { Link } from 'react-router'

const SortLoansLink = ({ sortParameter, children }) => (
  <Link
    to={sortParameter}
    activeStyle={{
      textDecoration: 'none',
      color: 'black'
    }}
  >
    {children}
  </Link>
)

export default SortLoansLink