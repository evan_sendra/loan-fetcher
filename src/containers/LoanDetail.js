import { connect } from 'react-redux'
import { fetchLoansIfNeeded, selectLoanId } from '../actions'
import Loan from '../components/Loan'
import _ from 'lodash'

const getSelectedLoan = (loans, selectedLoanId) => {
  let selectedLoan = _.cloneDeep(_.find(loans, {id: parseInt(selectedLoanId, 10)}))
  if (!selectedLoan) {
    return { "get_error": "Loan unavailable."}
  }
  return selectedLoan
}

const mapStateToProps = (state) => {
  return {
    selectedLoan: getSelectedLoan(state.loans.items, state.selectedLoanId)
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchLoansIfNeeded: (id) => {
      dispatch(selectLoanId(id))
      dispatch(fetchLoansIfNeeded())
    }
  }
}

const VisibleTodoList = connect(
  mapStateToProps,
  mapDispatchToProps
)(Loan)

export default VisibleTodoList