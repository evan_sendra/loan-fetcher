// const API = 'api.zonky.cz'
const API = 'evansendra.com/zonky'
export const API_DATA_URL = `https://${API}/loans/marketplace/`
export const API_BASE_URL = 'https://api.zonky.cz'

const FETCH_DELAY_IN_MINS = 5
export const STORY_DOESNT_EXIST = 'Příběh neexistuje'

// no need to edit below this line
const MILLIS_PER_MIN = 60000
export const APP_POLL_DELAY_MILLIS = FETCH_DELAY_IN_MINS * MILLIS_PER_MIN
