export const sampleApiData = [
  {
    id: 73269,
    name: "Refinancování půjček",
    story: " Dobrý den, Chtěl bych půjčené peníze použít na refinancování dvou půjček u Equa bank s RPSN 11,45%. Měsíčně bych ušetřil přibližně 3.400,- Kč a ještě bych přeplatil celkově shruba o 47.000,- méně. Což pro naší rodinu se třemi dětmi je hodně znát. Děkuji ",
    purpose: "6",
    photos: [
      {
        name: "6",
        url: "/loans/73269/photos/7094"
      }
    ],
    userId: 95135,
    nickName: "zonky95135",
    termInMonths: 84,
    interestRate: 0.0599,
    rating: "AAA",
    topped: null,
    amount: 360000,
    remainingInvestment: 298200,
    investmentRate: 0.17166666666666666,
    covered: false,
    datePublished: "2017-02-28T05:14:21.347+01:00",
    published: true,
    deadline: "2017-03-07T05:04:40.289+01:00",
    investmentsCount: 91,
    questionsCount: 2,
    region: "3",
    mainIncomeType: "SELF_EMPLOYMENT"
  },
  {
    id: 74914,
    name: "Překvapení, jedenkrát za život",
    story: "Ne často se sejde kulaté výročí svatby s výročím kulatým životním jubilejním. Lidé co si půjčují na dovolenou obvykle u mě neměli většího zastání (myslel jsem si, že nemají všech 1+1+1+1+1 pohromadě). Však jedenkrát za život investovat do exotické zahraniční dovolené, při všech jubileích, určitě stojí za to.",
    purpose: "3",
    photos: [
      {
        name: "3",
        url: "/loans/74914/photos/7096"
      }
    ],
    userId: 97671,
    nickName: "zonky97671",
    termInMonths: 84,
    interestRate: 0.0399,
    rating: "AAAAA",
    topped: null,
    amount: 200000,
    remainingInvestment: 174400,
    investmentRate: 0.128,
    covered: false,
    datePublished: "2017-02-28T08:23:06.584+01:00",
    published: true,
    deadline: "2017-03-07T08:14:33.920+01:00",
    investmentsCount: 43,
    questionsCount: 3,
    region: "9",
    mainIncomeType: "EMPLOYMENT"
  },
  {
    id: 75345,
    name: "Auto-moto",
    story: "Dobrý den, Jmenuji se Miloš, ale říkají mi Míša. Lidičky,.. chtěl bych Vás požádat o poskytnutí půjčky ve výši 245 000Kč. Touto cestou chci jednorázově splatit úvěr / leasing/od bankovní společnosti.Ta mi jej poskytla na financování mého nového vozidla zakoupeného v autobazaru. Staré vozidlo jsem dal na protiúčet a na zbylou částku 237000 mi byl poskytnut úvěr. Člověk si v tu radostnou chvíli ani neuvědomuje, co mu leasingovka nabízí za zlodějské úroky.. Jelikož bych u té společnosti za 84 měsíců přeplatil značnou částku a vozidlo by nebylo v mém vlastnictví, chtěl bych vás poprosit o investování do půjčky, kterou bych splatil již poskytnutý leasing. Víte, chtěl jsem si pořídit novější a bezpečnější vozidlo, které by mi vydrželo již na delší dobu. Své závazky jsem si vždy plnil včas a nikdy jsem se nezpozdil ve splátkách již poskytnutých půjček. Jsem státní zaměstnanec již 25 let a mám pravidelný měsíční příjem. Děkuji Vám milí investoři za projevenou důvěru a za možné poskytnutí půjčky.😉",
    purpose: "1",
    photos: [
      {
        name: "1",
        url: "/loans/75345/photos/7100"
      }
    ],
    userId: 98224,
    nickName: "zonky98224",
    termInMonths: 78,
    interestRate: 0.0599,
    rating: "AAA",
    topped: null,
    amount: 245000,
    remainingInvestment: 188000,
    investmentRate: 0.23265306122448978,
    covered: false,
    datePublished: "2017-02-28T12:26:10.000+01:00",
    published: true,
    deadline: "2017-03-07T12:13:22.623+01:00",
    investmentsCount: 74,
    questionsCount: 1,
    region: "13",
    mainIncomeType: "EMPLOYMENT"
  },
  {
    id: 75491,
    name: "Refinancování půjčky",
    story: "Dobrý den, milí investoři. Chtěl bych touto půjčkou refinancovat stávající půjčku u AirBank, kde je i se všemi bonusy RPSN na úrovni 7,12 %. Mám kromě půjčky (byla na auto) i hypotéku (zbývá pod dva miliony) - s tou jsem ale zatím docela spokojený. Jsem stabilně zaměstnaný jako vývojář (letos to bude 10 let), nikdy jsem neměl potíže se splácením ani opožděnou platbu. Mám finanční rezervy, hypotéku i život mám pojištěný. Jde mi tedy především o optimalizaci a neplýtvání penězi. Přijde mi zbytečné platit úroky bankám, když se to může dostat k lidem přímo. Děkuji za vyslechnutí. Přeji krásný den :)",
    purpose: "6",
    photos: [
      {
        name: "6",
        url: "/loans/75491/photos/7101"
      }
    ],
    userId: 98391,
    nickName: "jurri",
    termInMonths: 84,
    interestRate: 0.0499,
    rating: "AAAA",
    topped: null,
    amount: 325000,
    remainingInvestment: 295400,
    investmentRate: 0.09107692307692308,
    covered: false,
    datePublished: "2017-02-28T13:32:08.077+01:00",
    published: true,
    deadline: "2017-03-07T13:19:57.503+01:00",
    investmentsCount: 47,
    questionsCount: 0,
    region: "4",
    mainIncomeType: "EMPLOYMENT"
  },
  {
    id: 75371,
    name: "Domácnost",
    story: "Vážení a milí investoři, jmenuji se Zdeněk. Po dlouhých 5 letech jsem konečně dokončil stavbu domu a potřeboval bych ještě peníze na jeho vybavení. Proto bych se chtěl obrátit na vás. Doufám, že mi vyjdete vstříc. Moc vám děkuji.",
    purpose: "7",
    photos: [
      {
        name: "7",
        url: "/loans/75371/photos/7126"
      }
    ],
    userId: 98252,
    nickName: "zonky98252",
    termInMonths: 78,
    interestRate: 0.0599,
    rating: "AAA",
    topped: null,
    amount: 250000,
    remainingInvestment: 167000,
    investmentRate: 0.332,
    covered: false,
    datePublished: "2017-02-28T20:29:09.916+01:00",
    published: true,
    deadline: "2017-03-07T20:19:14.800+01:00",
    investmentsCount: 112,
    questionsCount: 1,
    region: "3",
    mainIncomeType: "EMPLOYMENT"
  },
  {
    id: 75036,
    name: "Rekonstrukce bytu",
    story: "Dobrý den milí investoři. Žiji s manželem, několika agamkami, jejich terárii a jedním kocourkem v pěkném bytě, který nám ovšem začal být malý, když se nám rozrostla rodina o krásného, dlouho očekávaného vnoučka. Takže nyní, když přijede na návštěvu dcera s rodinou, nemohou zde ani pohodlně přespat. Pořídili jsme tedy byt větší a začali s rekonstrukcí. Původní kalkulace, s kterou jsme počítali, je bohužel nedostačující. Určitý obnos sice získáme prodejem stávajícího bytu, ale nelze se stěhovat na staveniště. Plánuji využít i možnosti mimořádných splátek. Obracím se tedy na Vás s žádostí o půjčku na rekonstrukci bytu. Děkuji Vám za pomoc. ",
    purpose: "7",
    photos: [
      {
        name: "IMG-20140830-WA0002.jpg",
        url: "/loans/75036/photos/7129"
      }
    ],
    userId: 97826,
    nickName: "zonky97826",
    termInMonths: 60,
    interestRate: 0.0499,
    rating: "AAAA",
    topped: null,
    amount: 170000,
    remainingInvestment: 75400,
    investmentRate: 0.5564705882352942,
    covered: false,
    datePublished: "2017-02-28T21:48:52.197+01:00",
    published: true,
    deadline: "2017-03-07T21:43:03.328+01:00",
    investmentsCount: 130,
    questionsCount: 2,
    region: "6",
    mainIncomeType: "EMPLOYMENT"
  },
  {
    id: 75121,
    name: "Refinancování půjček",
    story: "Dobrý den, chtěla bych moc poprosit o pomoc s financováním této půjčky. Moc bych si přála snížit splátky našich stávajících úvěrů, abychom s manželem mohli finančně podpořit naší dceru v přestavbě bytu.Je ještě na škole a ikdyž se snaží a chodí na brigády....samozřejmě to nestačí.Manžel je řemeslník, ale bez peněz bohužel všechno udělat nejde.Pokud nám Vy - investoři pomůžete, budeme Vám moc vděčni. Děkuji G.",
    purpose: "6",
    photos: [
      {
        name: "file.png",
        url: "/loans/75121/photos/7140"
      }
    ],
    userId: 97937,
    nickName: "zonky97937",
    termInMonths: 78,
    interestRate: 0.0499,
    rating: "AAAA",
    topped: null,
    amount: 200000,
    remainingInvestment: 172400,
    investmentRate: 0.138,
    covered: false,
    datePublished: "2017-03-01T13:36:37.918+01:00",
    published: true,
    deadline: "2017-03-08T13:06:58.825+01:00",
    investmentsCount: 47,
    questionsCount: 0,
    region: "2",
    mainIncomeType: "EMPLOYMENT"
  },
  {
    id: 75831,
    name: "Domácnost",
    story: "Dobrý den, jmenuji se Jiří a je mi 31 let. S přítelkyní jsme minulý rok koupili byt 5+1 a pustili se do celkové rekonstrukce. Z vlastních zdrojů jsme do bytu vložili 400tis. a jsme jen krůček do konce. Chybí nám dokoupit vnitřní dveře, vybavení do obývacího pokoje, botník a věci drobného charakteru.(nové žaluzie, gárnyže atd.) Do bytu investujeme měsíčně cca. 15tis., ale plánujeme rodinu a rádi bychom se na přírůstek těšili v již zařízeném bytě a ušetřili si tak spoustu starostí a věnovali se pořizování výbavy pro potomka. Můj příjem je dostatečně vysoký a do schopnosti splácet půjčku je započítána i doba, po kterou bude přítelkyně (v tu dobu už nejspíš manželka) na mateřské dovolené. Mám velmi stabilní zaměstnání na slušné pozici a vzhledem k velkému personálnímu deficitu v našem sektoru i možnost neustálých služeb v případě potřeby. Rodina je pro mne, ale důležitější než trávit 300 hodin měsíčně prací. Velmi si vážíme Vaší pomoci a věříme, že touto cestou Vám Vaše proinvestované peníze vykouzlí i jemný úsměv na tváři a trochu radosti. Jěště jednou velmi děkujeme. . ",
    purpose: "7",
    photos: [
      {
        name: "7",
        url: "/loans/75831/photos/7166"
      }
    ],
    userId: 98776,
    nickName: "zonky98776",
    termInMonths: 66,
    interestRate: 0.0499,
    rating: "AAAA",
    topped: null,
    amount: 160000,
    remainingInvestment: 115200,
    investmentRate: 0.28,
    covered: false,
    datePublished: "2017-03-02T16:17:27.005+01:00",
    published: true,
    deadline: "2017-03-09T16:01:14.257+01:00",
    investmentsCount: 68,
    questionsCount: 2,
    region: "7",
    mainIncomeType: "EMPLOYMENT"
  },
  {
    id: 74751,
    name: "Zase se radovat",
    story: "Jsem kreativec tělem i duší...... Ahoj investoři :) a díky , že mi věnujete váš čas ;). Proč jsem se rozhodla, požádat vás o pomoc je touha věnovat se kreativní tvorbě naplno...A co mi v tom brání? Především čas a samozřejmě finance...Ale postupně. Tvořím od mala, tvořím s láskou a pro potěšení nejen mé, ale i všech těch, kterým jsem už mohla svými výrobky udělat radost. Pár roků na zpět jsem s dcerou začala podnikat v obchodě (v severních Čechách), s floristikou a aranžováním (na FB Květinářství U dvou sluníček). Rok a půl jsme se snažily získat každého nového zákazníka, dělaly jsme od svateb až po kreativní kurzy u nás v květinářství, ale bohužel, výše nájmu a špatně volený prostor, nás donutil obchod zavřít :(. A zůstaly dluhy a vzpomínky... Dcera se odstěhovala za přítelem do Prahy a já za přítelem k Pardubicím. Tehdy jsem měla dvě práce (dlouhá léta jsem souběžně s tím pracovala jako vedoucí oddělení péče pro zákazníky) a běhala z jedné práce do druhé. Byla jsem samoživitelka s nemalým nájmem. Bylo to vysilující, ale krásné, byť se špatným koncem.... Po přestěhování do Pardubic jsem byla nucena nastoupit do zaměstnaneckého poměru a momentálně jsem v situaci, že nemám čas ani peníze...Klasický \"krysí závod\", ale touha věnovat se dál mé tvorbě ( FB Zailla -Terapie/Tvorba) mě drží nad vodou. Nebudu se tady rozepisovat, v jakém kolotoči se teď nacházím a nebýt tvorby, bylo by to pro mě dost špatné. Zkrátka a dobře.....mám to vymyšlené asi tak... Potřebuju prostor, kde mohu své výrobky tvořit i nabízet, potřebuju se zbavit nevýhodného spotřebitelského úvěru a spojit vše se službou Nehtové modeláže ( kterou si chci co nejdřív udělat) a masážemi( mám rekvalifikaci a 2- letou praxi). Tím získám zákaznice a budu moct přímo prezentovat a nabízet i moje výrobky :). Reklama \"na živo\" je to nejlepší reklamou ;). Chci se věnovat tomu, co mě baví a naplňuje. Vážení investoři, příští týden mi bude 52 a čím dřív začnu, tím líp....Chci se ze života radovat ne žít v neustálém stresu :(. Pomůžete mi ? ",
    purpose: "6",
    photos: [
      {
        name: "IMG_20161102_164011.jpg",
        url: "/loans/74751/photos/7217"
      }
    ],
    userId: 97438,
    nickName: "Zailla",
    termInMonths: 66,
    interestRate: 0.0599,
    rating: "AAA",
    topped: null,
    amount: 170000,
    remainingInvestment: 109800,
    investmentRate: 0.35411764705882354,
    covered: false,
    datePublished: "2017-03-02T23:02:55.290+01:00",
    published: true,
    deadline: "2017-03-09T21:55:11.632+01:00",
    investmentsCount: 98,
    questionsCount: 0,
    region: "9",
    mainIncomeType: "EMPLOYMENT"
  },
  {
    id: 75760,
    name: "Refinancování půjček",
    story: "Vážení investoři, chci Vás požádat o poskytnutí finanční částky na refinancování mých půjček. Jedná se jednak o půjčku , kterou jsem si před lety vyřídil kvůli překlenutí více než rok trvajícího \"období bez zaměstnání\". Pracovní poměr jsem tehdy v srpnu 2008 ukončil po vzájemné dohodě se zaměstnavatelem kvůli neshodě s nadřízeným. To jsem však netušil, co nastane po pádu banky Lehman Brothers hned měsíc po ukončení mého pracovního poměru, tj. v září 2008. V následujícím období skutečně nebylo jednoduché sehnat novou práci, a tak jsem byl nucen využít on-line nabídky mé banky. Další půjčku jsem si pořídil před cca. třemi lety u jiného bankovního ústavu, pochopitelně za tehdy \"vynikajících\" podmínek, za účelem odkoupení malého rekreačního objektu - srubu - na místě, ke kterému jsem měl jistý citový vztah a hrozilo, že chajda padne do rukou nějaké jiné cizí nežádoucí osoby. V současné době jsem již osmým rokem součástí středního managementu německé firmy na výrobu elektronických součástek do domácích spotřebičů, kde jsem zaměstnán na dobu neurčitou. Spolu se svou partnerkou se staráme o 14letou dceru a nyní také o dvouměsíčního synka. O výše uvedené finanční prostředky Vás žádám z důvodu ozdravění našich financí. Své resty bych tak mohl urovnat o tři roky dříve a bankám bych nemusel platit kdysi možná velmi výhodné úroky. S přátelským pozdravem Josef Junek",
    purpose: "6",
    photos: [
      {
        name: "6",
        url: "/loans/75760/photos/7187"
      }
    ],
    userId: 98697,
    nickName: "zonky98697",
    termInMonths: 60,
    interestRate: 0.0599,
    rating: "AAA",
    topped: null,
    amount: 235000,
    remainingInvestment: 84600,
    investmentRate: 0.64,
    covered: false,
    datePublished: "2017-03-02T23:57:09.560+01:00",
    published: true,
    deadline: "2017-03-09T22:59:06.575+01:00",
    investmentsCount: 179,
    questionsCount: 1,
    region: "4",
    mainIncomeType: "EMPLOYMENT"
  },
  {
    id: 75800,
    name: "Dostavba bytové jednotky",
    story: "Vážení investoři, jmenuji se Petr a jsem státním zaměstnancem ve služebním poměru (pracuji na pozici vrchního ministerského rady - takhle krásně nás teď klasifikuje zákon o státní službě:-)). Zaměstnání mám stabilní a příjem nadprůměrný. Společně s rodinou dostavujeme přístavbu domu, ve kterém se nachází náš budoucí byt. Dům se nachází na Benešovsku a rádi bychom se tam cca do roka přestěhovali z Prahy, kde žijeme v současné době. Máme již z větší části hotovo, ale stále zbývá ještě spousta věcí na dodělání (koupelny, podlahy, dveře, kuchyně, dokončení fasády, balkóny a venkovní úpravy). Bohužel jsme si na dostavbu nemohli vzít výhodný hypoteční úvěr, protože v nemovitosti jsou z větší části nebytové prostory (restaurace a její zázemí) a pro banky jsou dle jejich metodik takové nemovitosti nevhodné do zástavy, a to i přesto, že mají jinak vysokou hodnotu. Museli jsme proto jít jinou cestou - nezajištěné úvěry. Částečně jsme využili úvěru ze stavebního spoření, kde jsme měli relativně hodně naspořeno (stavební spořitelna poskytla úvěr 500 tis. Kč na rekonstrukci bez zajištění s úrokem 2,95% - z toho cca 250 tis. Kč byly vlastní prostředky) - za to jsme dodělali střechu, komíny a z větší části zateplili fasádu. Tento úvěr je naší jedinou stávající půjčkou, kterou řádně splácíme. Dále jsme řešili, jak dofinancovat zbytek dostavby. V bance nám nabídli úvěr za 5,9%, což na dnešní poměry není úplně špatné, ale říkali jsme si, že zkusíme oslovit i Zonky a vyšlo to, dostali jsme nejlepší možný úrok, který v konkurenci s bankou je zajímavý. Chápu, že pro některé investory nemusí být tento úrok až tak atraktivní, ale myslím si, že v rámci diverzifikace rizika investičního portfolia jsou i takovéto příležitosti zajímavé a vyplatí se do nich investovat. Předem velmi děkuji všem za jejich nabídky. Zároveň velmi fandím celému týmu Zonky. Dosavadní přístup skvělý a hlavně férový. Moc jste mě překvapili!",
    purpose: "9",
    photos: [
      {
        name: "rekonstrukce.png",
        url: "/loans/75800/photos/7202"
      }
    ],
    userId: 98741,
    nickName: "olmega",
    termInMonths: 84,
    interestRate: 0.0399,
    rating: "AAAAA",
    topped: null,
    amount: 400000,
    remainingInvestment: 362600,
    investmentRate: 0.0935,
    covered: false,
    datePublished: "2017-03-03T16:41:52.853+01:00",
    published: true,
    deadline: "2017-03-10T11:26:20.675+01:00",
    investmentsCount: 49,
    questionsCount: 1,
    region: "1",
    mainIncomeType: "EMPLOYMENT"
  },
  {
    id: 75021,
    name: "Auto-moto",
    story: "Dobrý den investoři. Chtěl bych Vás tímto oslovil o zafinancování půjčky na auto. Momentálně využívám auto každý den na cestu do práce (denně 80 km), které mám v pronájmu od zaměstnavatele. Pronájem už pomalu končí a ja řeším co dále. Něco ne moc velkeho, svižneho jak po městě tak k dálničnímu použití. Také se chci trošku odlišit od běžných seriovek a tím padem to bude i takový mazlíček. Volba padla na Škodu Fabii RS. Nevím jestli seženu něco v TOP stavu u nás v ČR, ale asi ji nechám dovést z Německa. Něco o mě: 31 let, 13 let v automobilovém průmyslu. Ženatý, dvě děti. Stálý plat, hypotéka. Jinak nic. Půjčku mám v plánu určitě splatit dříve pomocí mimořádných splátek (vrácení daní, firemní bonusy 2 x ročně), moje představa 3 až 5 let. Pokud Vás cokoli zajímá, ptejte se. Děkuji. ",
    purpose: "1",
    photos: [
      {
        name: "4.jpg",
        url: "/loans/75021/photos/7206"
      }
    ],
    userId: 97806,
    nickName: "wolf1907",
    termInMonths: 84,
    interestRate: 0.0599,
    rating: "AAA",
    topped: null,
    amount: 170000,
    remainingInvestment: 126000,
    investmentRate: 0.25882352941176473,
    covered: false,
    datePublished: "2017-03-04T07:40:09.371+01:00",
    published: true,
    deadline: "2017-03-10T20:41:13.622+01:00",
    investmentsCount: 59,
    questionsCount: 0,
    region: "13",
    mainIncomeType: "EMPLOYMENT"
  },
  {
    id: 75936,
    name: "Refinancování půjček",
    story: "Vážení investoři, rádi bychom refinancovali půjčku, kterou jsme si brali při pořizování vlastního bydlení. Byli jsme v očekávání nového člena rodiny a v době jeho narození jsme již chtěli bydlet ve svém. Vysněný domeček máme, vysněného mrňouse taky, teď jen co nejdříve doplatit půjčku, ať mu můžeme domov zvelebit a pořídit mu vše, co bude potřebovat. Děkujeme",
    purpose: "6",
    photos: [
      {
        name: "DSCF1157.JPG",
        url: "/loans/75936/photos/7212"
      }
    ],
    userId: 98890,
    nickName: "Tomas32",
    termInMonths: 45,
    interestRate: 0.0599,
    rating: "AAA",
    topped: null,
    amount: 220000,
    remainingInvestment: 66400,
    investmentRate: 0.6981818181818182,
    covered: false,
    datePublished: "2017-03-04T21:44:34.861+01:00",
    published: true,
    deadline: "2017-03-11T21:40:47.188+01:00",
    investmentsCount: 238,
    questionsCount: 1,
    region: "11",
    mainIncomeType: "EMPLOYMENT"
  },
  {
    id: 75950,
    name: "Domek - zahrada",
    story: "Dobrý den, minulý rok se nám podařil dostavět domeček a letos bychom chtěli do léta dodělat zázemí pro našeho ročního synka, tak, aby se mohl v klidu proběhnout po zahradě. Děkuji za projevenou důvěru. Majka",
    purpose: "7",
    photos: [
      {
        name: "file.png",
        url: "/loans/75950/photos/7215"
      }
    ],
    userId: 98908,
    nickName: "MajkaZ",
    termInMonths: 54,
    interestRate: 0.1349,
    rating: "B",
    topped: null,
    amount: 40000,
    remainingInvestment: 0,
    investmentRate: 1,
    covered: true,
    datePublished: "2017-03-04T23:23:10.697+01:00",
    published: true,
    deadline: "2017-03-11T23:12:20.438+01:00",
    investmentsCount: 49,
    questionsCount: 0,
    region: "10",
    mainIncomeType: "MATERNITY_LEAVE"
  },
  {
    id: 75948,
    name: "Auto-moto",
    story: "Dobry den, peníze potřebujeme s manželkou na nákup auta abychom mohli dojíždět do zaměstnání, staré auto dojezdilo, oprava se nevyplatí a tak Vás žádáme o přispění, děkujeme Modrý Tomáš ",
    purpose: "1",
    photos: [
      {
        name: "1",
        url: "/loans/75948/photos/7210"
      }
    ],
    userId: 98906,
    nickName: "zonky98906",
    termInMonths: 54,
    interestRate: 0.0599,
    rating: "AAA",
    topped: null,
    amount: 70000,
    remainingInvestment: 0,
    investmentRate: 1,
    covered: true,
    datePublished: "2017-03-04T15:30:41.230+01:00",
    published: true,
    deadline: "2017-03-11T15:26:26.529+01:00",
    investmentsCount: 106,
    questionsCount: 0,
    region: "8",
    mainIncomeType: "EMPLOYMENT"
  },
  {
    id: 75292,
    name: "Refinancování půjčky",
    story: "Dobrý den, chtěl bych díky Vám refinancovat svojí aktuální půjčku, kterou jsme použil na nákup historického vozidla z období WW2. Konkrétně americký nákladní automobil GMC.",
    purpose: "6",
    photos: [
      {
        name: "IMG_20170227_093800.jpg",
        url: "/loans/75292/photos/7209"
      }
    ],
    userId: 98158,
    nickName: "zonky98158",
    termInMonths: 14,
    interestRate: 0.0599,
    rating: "AAA",
    topped: null,
    amount: 75000,
    remainingInvestment: 0,
    investmentRate: 1,
    covered: true,
    datePublished: "2017-03-04T10:53:10.587+01:00",
    published: true,
    deadline: "2017-03-11T10:44:11.073+01:00",
    investmentsCount: 106,
    questionsCount: 1,
    region: "10",
    mainIncomeType: "EMPLOYMENT"
  },
  {
    id: 74837,
    name: "Rodinná pohoda",
    story: "Vážení investoři, rád bych dokončil, co jsem loni začal, ale bohužel mi k tomu chybí žádané prostředky. Zázemí za domem a nově postavená chatka zeje prázdnotou, má ještě velké nedostatky. Rádi se u nás scházíme s rodinou a dobrými přáteli a tím mi pomůžete splnit si svůj sen. Moc děkuji Tomáš",
    purpose: "7",
    photos: [
      {
        name: "7",
        url: "/loans/74837/photos/7207"
      }
    ],
    userId: 97575,
    nickName: "TomTomTomio",
    termInMonths: 24,
    interestRate: 0.0599,
    rating: "AAA",
    topped: null,
    amount: 110000,
    remainingInvestment: 0,
    investmentRate: 1,
    covered: true,
    datePublished: "2017-03-04T10:16:17.384+01:00",
    published: true,
    deadline: "2017-03-11T09:57:53.012+01:00",
    investmentsCount: 149,
    questionsCount: 0,
    region: "5",
    mainIncomeType: "EMPLOYMENT"
  },
  {
    id: 75768,
    name: "Domácnost",
    story: null,
    purpose: "7",
    photos: [
      {
        name: "7",
        url: "/loans/75768/photos/7192"
      }
    ],
    userId: 98705,
    nickName: "MajkyMidas",
    termInMonths: 60,
    interestRate: 0.1999,
    rating: "D",
    topped: null,
    amount: 20000,
    remainingInvestment: 0,
    investmentRate: 1,
    covered: true,
    datePublished: "2017-03-04T09:50:00.069+01:00",
    published: true,
    deadline: "2017-03-10T09:47:15.126+01:00",
    investmentsCount: 22,
    questionsCount: 1,
    region: "13",
    mainIncomeType: "EMPLOYMENT"
  },
  {
    id: 75334,
    name: "Domácnost",
    story: "Žiji s přítelkyní v bytě 3+1 a po odchodu dětí jsme se rozhodli celý byt trochu poopravit.Poslední místností nám zbývá kuchyň.Máme svou,celkem skromnou, představu jak by to mělo vypadat a částečně si ji budeme financovat sami.Pro urychlení našeho záměru jsme se rozhodli o půjčku na Zonky jelikož nabízí dobré podmínky.Své závazky jsme vždy splatili včas a nikdy nikomu nedlužili.Díky za férové jednání.S pozdravem Míra a Marta.",
    purpose: "7",
    photos: [
      {
        name: "7",
        url: "/loans/75334/photos/7205"
      }
    ],
    userId: 98209,
    nickName: "zonky98209",
    termInMonths: 27,
    interestRate: 0.0599,
    rating: "AAA",
    topped: null,
    amount: 50000,
    remainingInvestment: 0,
    investmentRate: 1,
    covered: true,
    datePublished: "2017-03-03T23:51:37.206+01:00",
    published: true,
    deadline: "2017-03-10T23:31:35.946+01:00",
    investmentsCount: 87,
    questionsCount: 0,
    region: "6",
    mainIncomeType: "EMPLOYMENT"
  },
  {
    id: 74536,
    name: "Refinancování půjček",
    story: "Vazeni investori chtela bych vas poprosit o pomoc s refinancovanim pujcky. Pribeh me pujcky neni nijak zajimavy, ale rada bych ji doplatila co nejdrive, jelikoz je to zavazek, ktery mi brani ve vycestovani do sveta.",
    purpose: "6",
    photos: [
      {
        name: "6",
        url: "/loans/74536/photos/7204"
      }
    ],
    userId: 97023,
    nickName: "zonky97023",
    termInMonths: 22,
    interestRate: 0.1099,
    rating: "A",
    topped: null,
    amount: 125000,
    remainingInvestment: 0,
    investmentRate: 1,
    covered: true,
    datePublished: "2017-03-03T22:10:54.931+01:00",
    published: true,
    deadline: "2017-03-10T22:05:12.095+01:00",
    investmentsCount: 157,
    questionsCount: 0,
    region: "2",
    mainIncomeType: "EMPLOYMENT"
  }
]
