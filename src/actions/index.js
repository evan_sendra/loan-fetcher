import fetch from 'isomorphic-fetch'
import { API_DATA_URL, APP_POLL_DELAY_MILLIS } from '../constants'

// fetching loans from data source
export const REQUEST_LOANS = 'REQUEST_LOANS'
export const RECEIVE_LOANS = 'RECEIVE_LOANS'
export const INVALIDATE_LOANS = 'INVALIDATE_LOANS'

// user interactions
export const SET_POLL = 'SET_POLL'
export const SELECT_LOAN_ID = 'SELECT_LOAN_ID'

/**
 * @param selectedLoanId id of the loan for which we're viewing details
 */
export function selectLoanId (selectedLoanId) {
  return {
    type: SELECT_LOAN_ID,
    selectedLoanId
  }
}

export function setPoll () {
  return {
    type: SET_POLL
  }
}

export function invalidateLoans () {
  return {
    type: INVALIDATE_LOANS
  }
}

export function requestLoans () {
  return {
    type: REQUEST_LOANS
  }
}

function receiveLoans (json) {
  return {
    type: RECEIVE_LOANS,
    items: json ? json : [],
    receivedAt: Date.now()
  }
}

function fetchLoans () {
  return dispatch => {
    dispatch(requestLoans())
    return fetch(API_DATA_URL)
      .then(response => response.json())
      .then(json => dispatch(receiveLoans(json)))
      .catch(err => dispatch(receiveLoans([])))

  }
}

function delayReached (delayInMillis, lastUpdate) {
  return (new Date().getTime() - new Date(lastUpdate).getTime()) > (delayInMillis - 500)
}

function shouldFetchLoans (state) {
  if (!state.loans.items.length) {
    return true
  } else if (state.isFetching) {
    return false
  } else {
    return delayReached(APP_POLL_DELAY_MILLIS, state.loans.lastUpdated) && state.loans.didInvalidate
  }
}

export function fetchLoansIfNeeded () {
  return (dispatch, getState) => {
    if (shouldFetchLoans(getState()))
      return dispatch(fetchLoans())
  }
}
