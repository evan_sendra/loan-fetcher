/**
 * testable functions with business logic like sorting, story limiting
 */
import _ from 'lodash'
import { STORY_DOESNT_EXIST } from '../constants'

/**
 * The list of loans will contain a story _limited to 20 words with a maximum length of 200 characters_
 * @param story the original story text
 */
export const formatStory = (story) => {
  if (!story)
    return STORY_DOESNT_EXIST

  let limitedStory = story
  let trimmed = false // use this to add '...' to the end if needed

  // limit to 20 words
  if (limitedStory.split(' ').length > 20) {
    trimmed = true
    limitedStory = story.split(' ').slice(0, 20).join(' ')
  }
  // limit to 200 characters
  if (limitedStory.length > 200) {
    trimmed = true
    limitedStory = limitedStory.slice(0,200)
  }
  return trimmed ? (limitedStory + '...') : limitedStory
}

const stringComparator = (param) => {
  return (a,b) => a[param].localeCompare(b[param])
}

const dateComparator = (d1, d2) => {
  if (d1 < d2) {
    return -1
  } else if (d1 === d2) {
    return 0
  } else {
    return 1
  }
}

/**
 * @param loans list of loans with attributes to sort by
 * @param sortParameter the attribute by which to sort
 * @returns {*} loans sorted by sortParameter
 */
export const sortLoans = (loans, sortParameter) => {
  let loansCopy = _.cloneDeep(loans)
  switch (sortParameter) {
    case 'datePublished':
      return loansCopy.sort(stringComparator(sortParameter))
    case 'rating':
      return loansCopy.sort(stringComparator(sortParameter))
    case 'amount':
      return loansCopy.sort((a,b) => a[sortParameter] - b[sortParameter])
    case 'deadline':
      return loansCopy.sort((a,b) => dateComparator(a[sortParameter], b[sortParameter]))
    default:
      return loans
  }
}