import { createStore, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk'
import createLogger from 'redux-logger'
import loanFetcherApp from './reducers'

const loggerMiddleware = createLogger()

export default function configureStore (preloadedState) {
  return createStore(
      loanFetcherApp,
      preloadedState,
      applyMiddleware(
          thunkMiddleware,
          loggerMiddleware
      )
  )
}