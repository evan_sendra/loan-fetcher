import { combineReducers } from 'redux'
import {
  REQUEST_LOANS,
  RECEIVE_LOANS,
  INVALIDATE_LOANS,

  SET_POLL,
  SELECT_LOAN_ID
} from '../actions'

function selectedLoanId (state = NaN, action) {
  switch (action.type) {
    case SELECT_LOAN_ID:
      return  action.selectedLoanId
    default:
      return state
  }
}

function pollIsSet (state = false, action) {
  switch (action.type) {
    case SET_POLL:
      return true
    default:
      return state
  }
}

function loans (state = {
  lastUpdated: Date.now(),
  isFetching: false,
  didInvalidate: false,
  items: []
}, action) {
  switch (action.type) {
    case INVALIDATE_LOANS:
      return Object.assign({}, state, {
        didInvalidate: true
      })
    case REQUEST_LOANS:
      return Object.assign({}, state, {
        isFetching: true,
        didInvalidate: false
      })
    case RECEIVE_LOANS:
      return Object.assign({}, state, {
        isFetching: false,
        didInvalidate: false,
        items: action.items,
        lastUpdated: action.receivedAt
      })
    default:
      return state
  }
}

const loanFetcherApp = combineReducers({
  selectedLoanId,
  pollIsSet,
  loans
})

export default loanFetcherApp