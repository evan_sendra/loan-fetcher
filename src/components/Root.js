import React, { PropTypes } from 'react'
import { Provider } from 'react-redux'
import { Router, Route, browserHistory } from 'react-router'
import App from './App'
import LoanDetail from '../containers/LoanDetail'

import '../styles/App.css';

const Root = ({ store }) => (
    <Provider store={store}>
      <Router history={browserHistory} onUpdate={() => window.scrollTo(0,0)}>
        <Route path="(:sortParameter)" component={App} />
        <Route path="/loan/(:loanId)" component={LoanDetail} />
      </Router>
    </Provider>
)

Root.propTypes = {
  store: PropTypes.object.isRequired,
}

export default Root