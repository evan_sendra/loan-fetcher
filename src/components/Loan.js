import React, { PropTypes } from 'react'
import { browserHistory } from 'react-router'
import SafeImg from '../components/SafeImg'
import LoanTemplate from './LoanTemplate'
import { API_BASE_URL } from '../constants'
import '../styles/LoanDetail.css'

const ESC_KEY_CODE = 27

class Loan extends React.Component {
  // navigate user back home if they pressed escape
  keyPress (event) {
    if (event.keyCode === ESC_KEY_CODE || event.charCode === ESC_KEY_CODE) {
      browserHistory.push('/')
    }
  }

  componentWillMount () {
    document.addEventListener("keydown", this.keyPress, false)
    if (this.props.selectedLoan['get_error']) {
      this.props.fetchLoansIfNeeded(this.props.params.loanId)
    }
  }

  componentWillUnmount () {
    document.removeEventListener("keydown", this.keyPress, false)
  }

  render () {
    let loan = this.props.selectedLoan
    let loanAttributes = []
    for (let key of Object.keys(loan)) {
      if (loan.hasOwnProperty(key)) {
        if (key !== 'photos') {
          loanAttributes.push(
            <div key={key} className="loan_detail_item">
              <p>
                <span className="loan_detail_item_key">{String(key)}: </span>
                <span className="loan_detail_item_value">{String(loan[key])}</span>
              </p>
            </div>
          )
        }
      }
    }
    if (loan["get_error"]) {
      return (
        <LoanTemplate>
          <p>{loan['get_error']}</p>
        </LoanTemplate>
      )
    } else {
      return (
        <LoanTemplate>
          <SafeImg src={API_BASE_URL + loan['photos'][0]['url']} alt='' width='345' height='223' />
          {loanAttributes}
        </LoanTemplate>
      )
    }
  }
}

Loan.propTypes = {
  selectedLoan: PropTypes.shape({}).isRequired
}

export default Loan