import React from 'react'
import xImg from '../img/small-x.png'
import { browserHistory } from 'react-router'

const LoanTemplate = ({ children }) => {
  return (
    <div id="loan_detail">
      <a href="#" onClick={(e) => {e.preventDefault();browserHistory.goBack();}}>
        <img src={xImg} style={{float: 'right'}} alt="" width="50" height="50"/>
      </a>
      <h1>Loan Detail</h1>
      <div id="loan_properties">
        {children}
      </div>
    </div>
  )
}

export default LoanTemplate