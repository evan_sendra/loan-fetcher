import React from 'react'
import { PropTypes } from 'react'
import { API_BASE_URL } from '../constants'
import SafeImg from './SafeImg'
import { Link } from 'react-router'
import {formatStory} from '../utils'

const LoanList = ({ loans, onLoanClick }) => {
  let items = loans.items
  if (items.length) {
    return (
      <div id="loan_list">
        {items.map((loan,idx) =>
          <div key={loan.id}>
            <SafeImg src={API_BASE_URL + loan['photos'][0].url}
                     width="345"
                     height="223" />
            <h3>
              <Link onClick={() => onLoanClick(parseInt(loan.id, 10))} to={'/loan/' + loan.id}>
                {loan.name}
              </Link>
            </h3>
            <p>{formatStory(loan.story)}</p>
          </div>
        )}
      </div>
    )
  } else {
    return <p>No loans to show at this time.</p>
  }
}

LoanList.propTypes = {
  loans: PropTypes.shape({
    items: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired,
        photos: PropTypes.arrayOf(PropTypes.shape({
            url: PropTypes.string.isRequired
          }).isRequired
        ).isRequired,
        // story: PropTypes.string.isRequired
      }).isRequired
    ).isRequired
  }),
  onLoanClick: PropTypes.func.isRequired
}

export default LoanList