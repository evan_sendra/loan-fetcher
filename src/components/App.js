import React, { Component, PropTypes } from 'react';
import Header from './Header'
import SortedLoanList from '../containers/SortedLoanList'
import { invalidateLoans, fetchLoansIfNeeded, setPoll } from '../actions'
import { connect } from 'react-redux'
import { APP_POLL_DELAY_MILLIS } from '../constants'

class App extends Component {

  componentDidMount () {
    const { dispatch, pollIsSet } = this.props
    dispatch(fetchLoansIfNeeded())
    this.setupPoll(dispatch, pollIsSet)
  }

  setupPoll (dispatch, pollIsSet) {
    if (!pollIsSet) {
      dispatch(setPoll())
      window.setInterval(() => {
        dispatch(invalidateLoans())
        dispatch(fetchLoansIfNeeded())
      }, APP_POLL_DELAY_MILLIS)
    }
  }

  render () {
    const loans = this.props['loans']
    const { items, isFetching } = loans
    return (
      <div className="App">
        <h1>Marketplace Zonky</h1>
        <Header />
        {/*Page is loading for the first time*/}
        {isFetching && items.length === 0 &&
          <h2>Loading...</h2>
        }
        {/*Page isn't loading and there is no data*/}
        {!isFetching && items.length === 0 &&
            <div>
              No Loans to show.
            </div>
        }
        {/*Page is loaded or loading again*/}
        {items.length > 0 &&
          <div style={{ opacity: isFetching ? 0.5 : 1 }}>
            <SortedLoanList sortParameter={this.props.params.sortParameter || ''} />
          </div>
        }
      </div>
    )
  }
}

App.propTypes = {
  loans: PropTypes.shape({
    items: PropTypes.array.isRequired,
    lastUpdated: PropTypes.number.isRequired,
    didInvalidate: PropTypes.bool.isRequired,
    isFetching: PropTypes.bool.isRequired,
  }),
  selectedLoanId: PropTypes.number.isRequired,
}

function mapStateToProps (state) {
  const { loans, selectedLoanId, pollIsSet } = state

  return {
    loans,
    selectedLoanId,
    pollIsSet
  }
}

export default connect(mapStateToProps)(App)
