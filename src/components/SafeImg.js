import React from 'react'
import placeholder from '../img/placeholder.jpg'

export default class SafeImg extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      src: props.src,
      alt: props.alt || '',
      width: props.width,
      height: props.height
    }
  }

  onErr () {
    this.setState({
      src: placeholder
    })
  }

  render() {
    return <img src={this.state.src}
                alt={this.state.alt}
                width={this.state.width}
                height={this.state.height}
                onError={this.onErr} />
  }
}