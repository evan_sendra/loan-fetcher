import React from 'react'
import SortLoansLink from '../containers/SortLoansLink'

const Header = () => (
  <p>
    Sort by: {" "}
    <SortLoansLink sortParameter="datePublished">Date Published</SortLoansLink>{" | "}
    <SortLoansLink sortParameter="rating">Rating</SortLoansLink>{" | "}
    <SortLoansLink sortParameter="amount">Requested Amounts</SortLoansLink>{" | "}
    <SortLoansLink sortParameter="deadline">Deadline</SortLoansLink>
  </p>
)

export default Header