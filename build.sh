#!/bin/sh

yarn build
cp -r build zonky
zip -r zonky.zip zonky
scp -P 21186 zonky.zip evan@evansendra.com:/var/www/evansendra.com/
rm zonky.zip
rm -r zonky
