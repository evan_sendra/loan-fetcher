var express = require('express')
var cors = require('cors')
var app = express()
var request=require('request');
app.use(cors())

const API_URL = 'https://api.zonky.cz'

app.get('/zonky', (req,res) => {
	res.writeHead(200, { 'Content-Type': 'text/plain' })
	res.end('Routes under /loans/* proxy to api.zonky.cz')
})

app.get('/zonky/loans/*', (req, res) => {
	res.writeHead(200, { 'Content-Type': 'application/json' })
	request.get(API_URL + req.url.replace('/zonky',''),{},function(err,apiRes,body){
		res.end(body)
	});
})

app.listen(3000, function () {
})
